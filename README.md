# German Wings

## Par Hugo BRIATTE, Benjamin DEHEM et Virgil FORESTAL

Ce site internet est un site internet réalisé lors du semestre 1 a *l'Institut Universitaire de Technologie* de Lille en Informatique. Il a pour but de défendre une thèse complotiste. Nous avons du défendre le fait que le German Wings ne s'est pas crashé a cause d'une faute du pilote. Nous avons du ensuite réfuter dans une seconde partie cette theèse complotiste et y analyser les manières dont les complotistes avancent leur propos.

Ce projet avait pour but de nous initier premièrement au HTML et au CSS. Il a aussi été l'occasion pour nous de rédiger des articles en "vérifiant" nos sources et en essayant de convaincre en utilisant des procédés de rhétorique.
